@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">

            <!-- Default box -->
            <div class="box box-solid box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Поиск по базе</h3>
<!--                    <form action="search">-->
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Cервис</h3>
                                    <div class="box-tools">
                                        <div class="input-group input-group-sm" style="width: 150px;">
                                            <input type="text" name="search" class="form-control pull-right"
                                                   placeholder="Поиск">
                                            <div class="input-group-btn">
                                                <button type="submit" class="btn btn-default" ><i
                                                        class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover">
                                        <tr>
                                            <th>ID</th>
                                            <th>Наименование оборудования</th>
                                            <th>Серийный номер</th>
                                            <th>Описание неисправности</th>
                                            <th>Номер сервисного талона</th>
                                            <th>Дата передачи на сервис</th>
                                            <th>Текущий статус ремонта</th>
                                            <th>Дата последнего запроса</th>
                                            <th>Выполненные работы</th>
                                            <th>Дата оформления возврата</th>
                                        </tr>
                                        @foreach($b_ds as $BD)
                                        <tr>
                                            <th>{{$BD->equipment_identification}}</th>
                                            <th>{{$BD->serial_number}}</th>
                                            <th>{{$BD->malfunction_description}}</th>
                                            <th>{{$BD->service_ticket_number}}</th>
                                            <th>{{$BD->date_of_transfer_to_service}}</th>
                                            <th>{{$BD->current_repair_status}}</th>
                                            <th>{{$BD->repair_status}}</th>
                                            <th>{{$BD->completed_work}}</th>
                                            <th>{{$BD->return_date}}</th>
                                        </tr>
                                        @endforeach
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-danger">Удалить</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!--            </form>-->
            <div class="box box-solid box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Редактирование записей</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <form action="/"  method = "post" role="form">
                                @csrf
                                <div class="form-group">
                                    <label for="equipment_identification">Наименование оборудования:</label>
                                    <input type="text" class="form-control" id="equipment_identification" name = "$equipment_identification">
                                </div>
                                <div class="form-group">
                                    <label for="serial_number">Серийный номер:</label>
                                    <input type="text" class="form-control" id="serial_number" name = "$serial_number">
                                </div>
                                <div class="form-group">
                                    <label for="malfunction_description">Описание неисправности:</label>
                                    <textarea class="form-control" rows="5" id="malfunction_description" name = "$malfunction_description"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="service_ticket_number">Номер сервисного талона:</label>
                                    <input type="text" class="form-control" id="service_ticket_number" name = "$service_ticket_number">
                                </div>
                                <div class="form-group">
                                    <label for="date_of_transfer_to_service">Дата передачи на сервис:</label>
                                    <input type="text" class="form-control" id="date_of_transfer_to_service" name = "$date_of_transfer_to_service">
                                </div>
                                <div class="form-group">
                                    <label for="current_repair_status">Текущий статус ремонта:</label>
                                    <textarea class="form-control" rows="5" id="current_repair_status" name = "$current_repair_status"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="repair_status">Дата последнего запроса (статуса ремонта):</label>
                                    <input type="text" class="form-control" id="repair_status" name = "$repair_status">
                                </div>
                                <div class="form-group">
                                    <label for="completed_work">Выполненные работы:</label>
                                    <textarea class="form-control" rows="5" id="completed_work" name = "$completed_work"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="return_date">Дата оформления возврата:</label>
                                    <input type="text" class="form-control" id="return_date" name = "$return_date">
                                </div>
                                <button type="submit" class="btn btn-outline-info">Добавить</button>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
            @if (count($errors))
            <div class="form-group">
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach
                    </ul>
            </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
