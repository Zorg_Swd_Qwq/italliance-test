<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Http\Controllers\BD as b_ds;



class BD extends Model
{
    protected $fillable = ['equipment_identification', 'serial_number', 'malfunction_description', 'service_ticket_number', 'date_of_transfer_to_service', 'current_repair_status', 'repair_status', 'completed_work', 'return_date', 'user'
    ];

    protected $hidden = ['id', 'user',
    ];

    public function PostDataBase(){

        return $this->belongsToMany(b_ds::class);
    }
    protected $primaryKey = 'id';
    public $table = "b_ds";
    public $incrementing = true;
    public $timestamps = true;

}
