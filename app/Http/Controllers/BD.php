<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\BD as b_ds;


class BD extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $b_ds = b_ds::all();
        return view('home')->with('b_ds', $b_ds);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $PostView = new PostView([
            'equipment_identification' => $request -> post('equipment_identification'),
            'serial_number' => $request -> post('serial_number'),
            'malfunction_description' => $request -> post('malfunction_description'),
            'service_ticket_number' => $request -> post('service_ticket_number'),
            'date_of_transfer_to_service' => $request -> post('date_of_transfer_to_service'),
            'current_repair_status' => $request -> post('current_repair_status'),
            'repair_status' => $request -> post('repair_status'),
            'completed_work' => $request -> post('completed_work'),
            'return_date' => $request -> post('return_date')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
