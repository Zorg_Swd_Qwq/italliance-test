<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBDsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_ds', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('user');
            $table->char('equipment_identification');
            $table->integer('serial_number');
            $table->char('malfunction_description');
            $table->char('service_ticket_number');
            $table->date('date_of_transfer_to_service');
            $table->char('current_repair_status');
            $table->date('repair_status');
            $table->char('completed_work');
            $table->date('return_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_ds');
    }
}
